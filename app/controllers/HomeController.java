package controllers;

import play.mvc.*;

import views.html.*;

import javax.inject.Inject;

import play.db.*;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    private Database db;

    @Inject
    public HomeController(Database db) {
        this.db = db;
    }

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(index.render("Your new application is ready."));
    }

    /**
     *
     * */
    public Result update() {
        Connection connection = db.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from equipment limit 1");
            while(rs.next()) {
                BigInteger equipmentId = (BigInteger)rs.getObject("equipment_id");
                String equipmentType = (String)rs.getObject("equipment_type");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ok("{name: 'Zelimkhan'; age: 23; degree: 'Bachelor'}");
    }
}
