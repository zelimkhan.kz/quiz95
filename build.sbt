name := """quiz95"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs
)

libraryDependencies += "postgresql" % "postgresql" % "9.4.1212" from "https://jdbc.postgresql.org/download/postgresql-9.4.1212.jar"
